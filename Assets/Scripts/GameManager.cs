﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	public Transform targetsParent;
	public GameObject targetPrefab;
	public Player player;
	
	// Awake is called when the script instance is being loaded.
	protected void Awake()
	{
		instance = this;
	}
	
	// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	protected void Start()
	{
		for(int i = 0; i < 25; i++){
			GameObject target = Instantiate(targetPrefab,targetsParent);
			target.transform.localPosition = new Vector3(0,0,(i+1)*15);
			float randomRotation = Random.Range(-90f,90f);
			target.transform.localRotation = Quaternion.Euler(0,0,randomRotation);
		}
	}
	
	public void SetPlayerRotation(float rotation){
		player.SetRotation(rotation);
	}
}
