﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
	public Camera cam;
	
	private float zOffset;
	private Player player;
	
	// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	protected void Start()
	{
		player = GameManager.instance.player;
		zOffset = player.transform.position.z - transform.position.z;
	}
	
	// Update is called every frame, if the MonoBehaviour is enabled.
	protected void Update()
	{
		transform.position = new Vector3(transform.position.x,transform.position.y,player.transform.position.z - zOffset);
	}
	
}
