﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	public static InputManager instance;
	
	private float screenWidth;
	private float screenCutPercentage = 0.3f;
	private float screenMaxRight;
	private float screenMaxLeft;
	private float movementDistance;
	private float divisionFactor;
	private float clickPosX;
	private bool inputOpen = true;
	
	// Awake is called when the script instance is being loaded.
	protected void Awake()
	{
		instance = this;
		screenWidth = Screen.width;
		movementDistance = screenWidth * 0.3f;
		divisionFactor = movementDistance / 180; 
	}
	
	// Update is called every frame, if the MonoBehaviour is enabled.
	protected void Update()
	{
		if(inputOpen){
			if(Input.GetMouseButtonDown(0)){
				clickPosX = Input.mousePosition.x;
				screenMaxLeft = clickPosX - movementDistance/2;
				screenMaxRight = clickPosX + movementDistance/2;
			}
			
			if(Input.GetMouseButton(0)){
				
				
				float mouseX = Input.mousePosition.x;
				//mouseX = Mathf.Clamp(mouseX,screenMaxLeft,screenMaxRight);
				float distanceFromMiddle = mouseX - clickPosX;
				float rotation = distanceFromMiddle/divisionFactor;
				GameManager.instance.SetPlayerRotation(rotation);
				
				clickPosX = Input.mousePosition.x;
				screenMaxLeft = clickPosX - movementDistance/2;
				screenMaxRight = clickPosX + movementDistance/2;
			}
		}
	}
	
	public void SetInputOpen(bool b){
		inputOpen = b;
	}
	
	public bool GetInputOpen(){
		return inputOpen;
	}
	
}
