﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
	    transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.z + 10*Time.deltaTime);
    }
    
	public void SetRotation(float rotation){
		float targetRotation = transform.localRotation.eulerAngles.z;
		if(targetRotation > 180){
			targetRotation -= 360;
		}
		targetRotation += rotation;
		targetRotation = Mathf.Clamp(targetRotation,-90,90);
		transform.localRotation = Quaternion.Euler(0,0,targetRotation);
	}
}
